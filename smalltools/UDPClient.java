import java.awt.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.io.InterruptedIOException;


public class UDPClient
{
	public static void main(String[] args) throws IOException
	{
		int port = -1, s_port = -1, count = 0;
		String s_hostname = "";
		for (int i = 0; i < args.length; i++)
		{
			String arg = args[i];
			if (arg.equals("-l"))
			{
				try
				{
					port = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else if (arg.equals("-h"))
			{
				s_hostname = args[++i];
			}
			else if (arg.equals("-r"))
			{
				try
				{
					s_port = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else if (arg.equals("-c"))
			{
				try
				{
					count = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else
			{
				System.out.println("Error: missing or additional arguments");
				System.exit(0);
			}
		}

		InetAddress server = InetAddress.getByName(s_hostname);
		DatagramSocket socket = new DatagramSocket(port);
		socket.setSoTimeout(1000000);
		long min = 1000000, max = 0, sum = 0;
		int loss = 0;
		for (int seq = 0; seq < count; seq++)
		{
			byte[] data = new byte[12];
			long stamp = System.currentTimeMillis();
			for (int i = 0; i < 4; i++)
				data[i] = (byte) ((seq >> (8 * (3 - i))) & 0xFF);
			for (int i = 0; i < 8; i++)
				data[i + 4] = (byte) ((stamp >> (8 * (7 - i))) & 0xFF);


			DatagramPacket packet = new DatagramPacket(data, data.length, server, s_port);

			socket.send(packet);


			byte[] response = new byte[1024];
			DatagramPacket reply = new DatagramPacket(response, response.length);

			try
			{
				socket.receive(reply);
			}
			catch (InterruptedIOException e)
			{
				System.out.println("time out");
				loss++;
				continue;
			}
			byte[] data2 = reply.getData();

			int s_seq = 0;
			long s_stamp = 0;
			for (int i = 0; i < 4; i++)
			{
				s_seq <<= 8;
				s_seq |= data2[i] & 0xff;
			}
			for (int i = 0; i < 8; i++)
			{
				s_stamp <<= 8;
				s_stamp |= data2[i + 4] & 0xff;
			}
			long delta = System.currentTimeMillis() - s_stamp;
			if (delta < min)
				min = delta;
			if (delta > max)
				max = delta;
			sum += delta;
			System.out.println("size=" + reply.getLength() + " from=" + reply.getAddress() + " seq=" + s_seq + " rtt=" + (System.currentTimeMillis() - s_stamp));
		}
		System.out.println("sent=" + count + " received=" + (count - loss) + " lost=" + ((float) (loss) / count) + " rtt min/avg/max=" + min + '/' + ((float) (sum) / (count - loss)) + '/' + max + "ms");
		socket.close();
	}
}