import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer
{
	public static void main(String[] args) throws IOException
	{
		int port = -1;
		for (int i = 0; i < args.length; i++)
		{
			String arg = args[i];
			if (arg.equals("-l"))
			{
				try
				{
					port = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else
			{
				System.out.println("Error: missing or additional arguments");
				System.exit(0);
			}
		}

		if (port <= 1024 || port >= 65536)
		{
			System.out.println("Error: missing or additional arguments");
			System.exit(0);
		}

		ServerSocket serverSocket = new ServerSocket(port);
		Socket socket = serverSocket.accept();
		InputStream is = socket.getInputStream();
		byte[] data = new byte[2048];
		int count = 0;
		long st1 = System.currentTimeMillis();
		while (true)
		{
			try
			{
				if (is.read(data) != -1)
					count++;
				else
					break;
			}
			catch (Exception e)
			{
				System.out.println(e);
				break;
			}
		}
		long st2 = System.currentTimeMillis();
		System.out.println("received=" + count + " KB rate=" + ((float) (count) / (st2 - st1) * 1000) + " Mbps");
		socket.close();
	}


}