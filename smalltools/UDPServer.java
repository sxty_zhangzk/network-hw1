import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class UDPServer
{
	public static void main(String[] args) throws IOException
	{
		int port = -1;
		for (int i = 0; i < args.length; i++)
		{
			String arg = args[i];
			if (arg.equals("-l"))
			{
				try
				{
					port = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else
			{
				System.out.println("Error: missing or additional arguments");
				System.exit(0);
			}
		}

		if (port <= 1024 || port >= 65536)
		{
			System.out.println("Error: missing or additional arguments");
			System.exit(0);
		}

		DatagramSocket socket = new DatagramSocket(port);

		byte[] data = new byte[1024];
		DatagramPacket packet = new DatagramPacket(data, data.length);
		while (true)
		{

			socket.receive(packet);

			byte[] temp = packet.getData();
			InetAddress address = packet.getAddress();
			int seq = 0;
			for (int i = 0; i < 4; i++)
			{
				seq <<= 8;
				seq |= temp[i] & 0xff;
			}
			long timestamp = System.currentTimeMillis();
			System.out.println("time=" + timestamp + " from=" + address.toString() + " seq=" + seq);

			int c_port = packet.getPort();

			DatagramPacket reply = new DatagramPacket(data, data.length, address, c_port);

			socket.send(reply);
		}
		//socket.close();
	}


}