import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.OutputStream;
import java.awt.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.io.InterruptedIOException;

public class TCPClient
{
	public static void main(String[] args) throws IOException
	{
		int s_port = -1, time = 0;
		String s_hostname = "";
		for (int i = 0; i < args.length; i++)
		{
			String arg = args[i];
			if (arg.equals("-t"))
			{
				try
				{
					time = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else if (arg.equals("-h"))
			{
				s_hostname = args[++i];
			}
			else if (arg.equals("-p"))
			{
				try
				{
					s_port = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else if (arg.equals("-c"))
			{

			}
			else
			{
				System.out.println("Error: missing or additional arguments");
				System.exit(0);
			}
		}

		InetAddress server = InetAddress.getByName(s_hostname);
		Socket socket = new Socket(server, s_port);
		byte[] data = new byte[1024];
		OutputStream os = socket.getOutputStream();
		long st1 = System.currentTimeMillis();
		long st2 = 0;
		int count = 0;
		while (true)
		{
			try
			{
				st2 = System.currentTimeMillis();
				if (st2 - st1 > time * 1000)
					break;
				os.write(data);
				count++;
			}
			catch (Exception e)
			{
				System.out.println(e);
				break;
			}
		}
		System.out.println("sent=" + count + " KB rate=" + ((float) (count) / (st2 - st1) * 1000) + " Mbps");
		socket.close();
	}

}