import java.net.InetAddress;
import java.net.Socket;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;

public class iperfer
{
	public static void main(String[] args) throws IOException
	{
		int port = -1, time = 0;
		String s_hostname = "";
		boolean is_server = true;
		for (int i = 0; i < args.length; i++)
		{
			String arg = args[i];
			if (arg.equals("-t"))
			{
				try
				{
					time = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else if (arg.equals("-h"))
			{
				s_hostname = args[++i];
			}
			else if (arg.equals("-p"))
			{
				try
				{
					port = Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: missing or additional arguments");
					System.exit(0);
				}
			}
			else if (arg.equals("-c"))
			{
				is_server = false;
			}
			else if (arg.equals("-s"))
			{
				is_server = true;
			}
			else
			{
				System.out.println("Error: missing or additional arguments");
				System.exit(0);
			}
		}
		if (is_server)
		{
			if (port <= 1024 || port >= 65536)
			{
				System.out.println("Error: missing or additional arguments");
				System.exit(0);
			}

			ServerSocket serverSocket = new ServerSocket(port);
			while (true)
			{
				Socket socket = serverSocket.accept();
				InputStream is = socket.getInputStream();
				byte[] data = new byte[2048];
				int count = 0;
				long st1 = System.currentTimeMillis();
				while (true)
				{
					try
					{
						int len = is.read(data);
						if (len != -1)
							count += len;
						else
							break;
					}
					catch (Exception e)
					{
						System.out.println(e);
						break;
					}
				}
				long st2 = System.currentTimeMillis();
				System.out.println("received=" + (float) (count) / 1024 + " KB rate=" + ((float) (count) / 1024 / (st2 - st1) * 1000 / 1024 * 8) + " Mbps");
				socket.close();
			}
		}
		else
		{
			InetAddress server = InetAddress.getByName(s_hostname);
			Socket socket = new Socket(server, port);
			byte[] data = new byte[1024];
			OutputStream os = socket.getOutputStream();
			long st1 = System.currentTimeMillis();
			long st2 = 0;
			int count = 0;
			while (true)
			{
				try
				{
					st2 = System.currentTimeMillis();
					if (st2 - st1 > time * 1000)
						break;
					os.write(data);
					count++;
				}
				catch (Exception e)
				{
					System.out.println(e);
					break;
				}
			}
			System.out.println("sent=" + count + " KB rate=" + ((float) (count) / (st2 - st1) * 1000 / 1024 * 8) + " Mbps");
			socket.close();
		}
	}
}