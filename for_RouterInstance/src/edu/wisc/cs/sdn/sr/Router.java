package edu.wisc.cs.sdn.sr;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import edu.wisc.cs.sdn.sr.vns.VNSComm;

import net.floodlightcontroller.packet.IPacket;
import net.floodlightcontroller.packet.Data;
import net.floodlightcontroller.packet.ARP;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.ICMP;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.UDP;
import net.floodlightcontroller.util.MACAddress;

/**
 * @author Aaron Gember-Jacobson
 */
public class Router
{
	public static final byte[] BROADCAST_MAC = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};

	/**
	 * User under which the router is running
	 */
	private String user;

	/**
	 * Hostname for the router
	 */
	private String host;

	/**
	 * Template name for the router; null if no template
	 */
	private String template;

	/**
	 * Topology ID for the router
	 */
	private short topo;

	/**
	 * List of the router's interfaces; maps interface name's to interfaces
	 */
	private Map<String, Iface> interfaces;

	/**
	 * Routing table for the router
	 */
	private RouteTable routeTable;

	/**
	 * ARP cache for the router
	 */
	private ArpCache arpCache;

	/**
	 * PCAP dump file for logging all packets sent/received by the router;
	 * null if packets should not be logged
	 */
	private DumpFile logfile;

	/**
	 * Virtual Network Simulator communication manager for the router
	 */
	private VNSComm vnsComm;

	/**
	 * RIP subsystem
	 */
	private RIP rip;

	/**
	 * Creates a router for a specific topology, host, and user.
	 *
	 * @param topo     topology ID for the router
	 * @param host     hostname for the router
	 * @param user     user under which the router is running
	 * @param template template name for the router; null if no template
	 */
	public Router(short topo, String host, String user, String template)
	{
		this.topo = topo;
		this.host = host;
		this.setUser(user);
		this.template = template;
		this.logfile = null;
		this.interfaces = new HashMap<String, Iface>();
		this.routeTable = new RouteTable();
		this.arpCache = new ArpCache(this);
		this.vnsComm = null;
		this.rip = new RIP(this);
	}

	public void init()
	{
		this.rip.init();
	}

	/**
	 * @param logfile PCAP dump file for logging all packets sent/received by
	 *                the router; null if packets should not be logged
	 */
	public void setLogFile(DumpFile logfile)
	{
		this.logfile = logfile;
	}

	/**
	 * @return PCAP dump file for logging all packets sent/received by the
	 * router; null if packets should not be logged
	 */
	public DumpFile getLogFile()
	{
		return this.logfile;
	}

	/**
	 * @param template template name for the router; null if no template
	 */
	public void setTemplate(String template)
	{
		this.template = template;
	}

	/**
	 * @return template template name for the router; null if no template
	 */
	public String getTemplate()
	{
		return this.template;
	}

	/**
	 * @param user user under which the router is running; if null, use current
	 *             system user
	 */
	public void setUser(String user)
	{
		if (null == user)
		{
			this.user = System.getProperty("user.name");
		}
		else
		{
			this.user = user;
		}
	}

	/**
	 * @return user under which the router is running
	 */
	public String getUser()
	{
		return this.user;
	}

	/**
	 * @return hostname for the router
	 */
	public String getHost()
	{
		return this.host;
	}

	/**
	 * @return topology ID for the router
	 */
	public short getTopo()
	{
		return this.topo;
	}

	/**
	 * @return routing table for the router
	 */
	public RouteTable getRouteTable()
	{
		return this.routeTable;
	}

	/**
	 * @return list of the router's interfaces; maps interface name's to
	 * interfaces
	 */
	public Map<String, Iface> getInterfaces()
	{
		return this.interfaces;
	}

	/**
	 * @param vnsComm Virtual Network System communication manager for the router
	 */
	public void setVNSComm(VNSComm vnsComm)
	{
		this.vnsComm = vnsComm;
	}

	/**
	 * Close the PCAP dump file for the router, if logging is enabled.
	 */
	public void destroy()
	{
		if (logfile != null)
		{
			this.logfile.close();
		}
	}

	/**
	 * Load a new routing table from a file.
	 *
	 * @param routeTableFile the name of the file containing the routing table
	 */
	public void loadRouteTable(String routeTableFile)
	{
		if (!routeTable.load(routeTableFile))
		{
			System.err.println("Error setting up routing table from file " + routeTableFile);
			System.exit(1);
		}

		System.out.println("Loading routing table");
		System.out.println("---------------------------------------------");
		System.out.print(this.routeTable.toString());
		System.out.println("---------------------------------------------");
	}

	/**
	 * Add an interface to the router.
	 *
	 * @param ifaceName the name of the interface
	 */
	public Iface addInterface(String ifaceName)
	{
		Iface iface = new Iface(ifaceName);
		this.interfaces.put(ifaceName, iface);
		return iface;
	}

	/**
	 * Gets an interface on the router by the interface's name.
	 *
	 * @param ifaceName name of the desired interface
	 * @return requested interface; null if no interface with the given name
	 * exists
	 */
	public Iface getInterface(String ifaceName)
	{
		return this.interfaces.get(ifaceName);
	}

	/**
	 * Send an Ethernet packet out a specific interface.
	 *
	 * @param etherPacket an Ethernet packet with all fields, encapsulated
	 *                    headers, and payloads completed
	 * @param iface       interface on which to send the packet
	 * @return true if the packet was sent successfully, otherwise false
	 */
	public boolean sendPacket(Ethernet etherPacket, Iface iface)
	{
		return this.vnsComm.sendPacket(etherPacket, iface.getName());
	}

	/**
	 * Handle an Ethernet packet received on a specific interface.
	 *
	 * @param etherPacket the Ethernet packet that was received
	 * @param inIface     the interface on which the packet was received
	 */
	public void handlePacket(Ethernet etherPacket, Iface inIface)
	{
		System.out.println("*** -> Received packet: " + etherPacket.toString().replace("\n", "\n\t"));

		/********************************************************************/
		/* Handle packets                                                   */
		/********************************************************************/

		if(!Arrays.equals(etherPacket.getDestinationMAC().toBytes(), inIface.getMacAddress().toBytes())
				&& !etherPacket.getDestinationMAC().isBroadcast())
		{
			return;
		}

		if(etherPacket.getEtherType() == Ethernet.TYPE_ARP)
		{
			handleArpPacket(etherPacket, inIface);
			return;
		}

		if(etherPacket.getEtherType() == Ethernet.TYPE_IPv4)
		{
			IPv4 ipv4Packet = (IPv4) etherPacket.getPayload();
			//TODO: checksum

			routePacket(ipv4Packet, inIface);
			return;
		}
	}

	/**
	 * Check if an IP address belongs to the router itself.
	 *
	 * @param ipaddr an IP address
	 * @return true if one of the interfaces on the router has address ipaddr, otherwise false.
	 */
	public boolean isLocalAddress(int ipaddr)
	{
		for(Map.Entry<String, Iface> entry : getInterfaces().entrySet())
		{
			if(entry.getValue().getIpAddress() == ipaddr)
				return true;
		}
		return false;
	}

	/**
	 * Check if an IP address is on a subnet directly connected to the router.
	 *
	 * @param ipaddr an IP address
	 * @return true if ipaddr is on local subnet
	 */
	public boolean isLocalSubnet(int ipaddr)
	{
		for(Map.Entry<String, Iface> entry : getInterfaces().entrySet())
		{
			int mask = entry.getValue().getSubnetMask();
			if((entry.getValue().getIpAddress() & mask) == (ipaddr & mask))
				return true;
		}
		return false;
	}

	/**
	 * Check if an IP address is a multicast address (currently only support RIP multicast)
	 * @param ipaddr an IP address
	 * @return true if ipaddr is an multicast address
	 */
	public boolean isMulticastAddress(int ipaddr)
	{
		return ipaddr == RIP.RIP_MULTICAST_IP;
	}

	/**
	 * Route the received packet: check the route table, reduce the TTL and forward it to the next hop.
	 *
	 * @param ipv4Packet the received IP packet
	 * @param inIface the interface on which the packet was received
	 */
	private void routePacket(IPv4 ipv4Packet, Iface inIface)
	{
		System.out.println("****** -> routePacket: " + ipv4Packet.toString());

		// Input Route
		if(isLocalAddress(ipv4Packet.getDestinationAddress()))
		{
			inputPacket(ipv4Packet, inIface);
			return;
		}

		if(isMulticastAddress(ipv4Packet.getDestinationAddress()))
		{
			inputPacket(ipv4Packet, inIface);
			return;
		}

		// Forward Route
		RouteTableEntry routeEntry = routeTable.matchEntry(ipv4Packet.getDestinationAddress());

		if(routeEntry == null)
		{
			// DESTINATION NETWORK UNREACHABLE
			handleFailure(ipv4Packet, ICMP.TYPE_UNREACHABLE, ICMP.CODE_NET_UNREACHABLE);
			return;
		}

		byte ttl = ipv4Packet.getTtl();
		if(ttl == 1 || ttl == 0)
		{
			// TTL Limit Exceeded
			handleFailure(ipv4Packet, ICMP.TYPE_TIME_EXCEEDED, (byte)0);
			return;
		}
		ipv4Packet.setTtl((byte)(ttl - 1));

		postPacket(ipv4Packet, routeEntry);
	}

	/**
	 * Handle the packet of which the destination is the router itself.
	 *
	 * @param ipv4Packet the received IP packet
	 * @param inIface the interface on which the packet was received
	 */
	private void inputPacket(IPv4 ipv4Packet, Iface inIface)
	{
		boolean portReachable = true;
		if(ipv4Packet.getProtocol() == IPv4.PROTOCOL_ICMP)
		{
			ICMP icmpPacket = (ICMP) ipv4Packet.getPayload();
			portReachable = handleICMP(icmpPacket, ipv4Packet.getSourceAddress(), ipv4Packet.getDestinationAddress());
		}
		else if(ipv4Packet.getProtocol() == IPv4.PROTOCOL_UDP)
		{
			UDP udpPacket = (UDP)ipv4Packet.getPayload();
			portReachable = handleUDP(udpPacket, ipv4Packet.getSourceAddress(), ipv4Packet.getDestinationAddress(), inIface);
		}
		else if(ipv4Packet.getProtocol() == IPv4.PROTOCOL_TCP)
		{
			portReachable = false;
		}

		if(!portReachable)
		{
			handleFailure(ipv4Packet, ICMP.TYPE_UNREACHABLE, ICMP.CODE_PORT_UNREACHABLE);
		}
	}

	/**
	 * Send an IP packet from the router
	 * @param ipv4Packet an IP packet with all fields (the source field can be 0 to let the system select one automatically)
	 */
	private void outputPacket(IPv4 ipv4Packet)
	{
		if(isMulticastAddress(ipv4Packet.getDestinationAddress()))
		{
			for(Map.Entry<String, Iface> entry : interfaces.entrySet())
			{
				Iface outIface = entry.getValue();
				RouteTableEntry routeEntry = new RouteTableEntry(ipv4Packet.getDestinationAddress(), 0, ~0, entry.getKey(), 0, -1);
				ipv4Packet.setSourceAddress(outIface.getIpAddress());
				postPacket((IPv4)ipv4Packet.clone(), routeEntry);
			}
			return;
		}

		// Output Route
		RouteTableEntry routeEntry = routeTable.matchEntry(ipv4Packet.getDestinationAddress());
		if(routeEntry == null)
			return;

		Iface outIface = interfaces.get(routeEntry.getInterface());
		assert(outIface != null);

		if(ipv4Packet.getSourceAddress() == 0)
			ipv4Packet.setSourceAddress(outIface.getIpAddress());

		postPacket(ipv4Packet, routeEntry);
	}

	/**
	 * Send an IPv4 packet after routing
	 * @param ipv4Packet an IPv4 packet with all fields
	 * @param routeEntry route entry to send the packet
	 */
	private void postPacket(IPv4 ipv4Packet, RouteTableEntry routeEntry)
	{
		System.out.println("****** -> postPacket: " + ipv4Packet.toString());

		if(isLocalAddress(ipv4Packet.getDestinationAddress()))
		{
			// Loopback packet
			// TODO: Handle loopback
			return;
		}

		ipv4Packet.resetChecksum();

		int dstIp = ipv4Packet.getDestinationAddress();
		if(routeEntry.getGatewayAddress() != 0)
			dstIp = routeEntry.getGatewayAddress();

		Iface outIface = interfaces.get(routeEntry.getInterface());
		assert(outIface != null);

		Ethernet etherPacket = new Ethernet();
		etherPacket.setSourceMACAddress(outIface.getMacAddress().toBytes());
		etherPacket.setEtherType(Ethernet.TYPE_IPv4);
		etherPacket.setPayload(ipv4Packet);

		if(isMulticastAddress(ipv4Packet.getDestinationAddress()))
		{
			etherPacket.setDestinationMACAddress(BROADCAST_MAC);
			sendPacket(etherPacket, outIface);
			return;
		}

		ArpEntry arpEntry = arpCache.lookup(dstIp);
		if(arpEntry == null)
		{
			arpCache.waitForArp(etherPacket, outIface, dstIp);
			return;
		}

		etherPacket.setDestinationMACAddress(arpEntry.getMac().toBytes());
		sendPacket(etherPacket, outIface);
	}

	/**
	 * Handle the received ICMP packet
	 * @param icmpPacket the received ICMP packet
	 * @param srcIp the source address of the packet
	 * @param dstIp the destination address of the packet
	 * @return true if PORT UNREACHABLE occurred, otherwise false
	 */
	private boolean handleICMP(ICMP icmpPacket, int srcIp, int dstIp)
	{
		if(icmpPacket.getIcmpType() == ICMP.TYPE_ECHO_REQUEST)
		{
			sendPacketICMP(ICMP.TYPE_ECHO_REPLY, (byte)0, icmpPacket.getPayload(), srcIp, dstIp);
		}
		return true;
	}

	/**
	 * Handle the received UDP packet
	 * @param udpPacket the received UDP packet
	 * @param srcIp the source address of the packet
	 * @param dstIp the destination address of the packet
	 * @param inIface the interface on which the packet was received
	 * @return true if PORT UNREACHABLE occurred, otherwise false
	 */
	private boolean handleUDP(UDP udpPacket, int srcIp, int dstIp, Iface inIface)
	{
		if(dstIp == RIP.RIP_MULTICAST_IP && udpPacket.getDestinationPort() == UDP.RIP_PORT)
		{
			rip.handlePacket(udpPacket, srcIp, inIface);
			return true;
		}
		return false;
	}

	/**
	 * Reply an ICMP error message to the sender when an error occurred
	 *
	 * @param originalPacket the original packet the router received
	 * @param icmpType the ICMP Type field of the ICMP packet
	 * @param icmpCode the ICMP Code field of the ICMP packet
	 */
	private void handleFailure(IPv4 originalPacket, byte icmpType, byte icmpCode)
	{
		int srcIp = 0;
		if(isLocalAddress(originalPacket.getDestinationAddress()))
			srcIp = originalPacket.getDestinationAddress();

		byte[] data = originalPacket.serialize();
		if(data.length >= 28)
		{
			// the option field of the ICMP packet should be filled with the first 28 bytes (20 bytes header + first 8 bytes payload)
			//  to help the OS to identify sender process
			byte[] payloadData = new byte[4 + 28];
			for(int i=0; i<28; i++)
				payloadData[i+4] = data[i];
			sendPacketICMP(icmpType, icmpCode, new Data(payloadData), originalPacket.getSourceAddress(), srcIp);
		}
		else
		{
			sendPacketICMP(icmpType, icmpCode, null, originalPacket.getSourceAddress(), srcIp);
		}
	}

	/**
	 * Send an L4 (transport layer) packet to the destination
	 * @param l4Packet the L4 packet which is about to send
	 * @param protocol the protocol of the L4 packet
	 * @param dstIp the destination of the IP packet
	 * @param srcIp the source of the IP packet, can be 0 to select one automatically
	 */
	public void sendPacketL4(IPacket l4Packet, byte protocol, int dstIp, int srcIp)
	{
		IPv4 ipv4Packet = new IPv4();
		ipv4Packet.setSourceAddress(srcIp);
		ipv4Packet.setDestinationAddress(dstIp);
		ipv4Packet.setTtl((byte)64);
		ipv4Packet.setProtocol(protocol);
		ipv4Packet.setPayload(l4Packet);
		outputPacket(ipv4Packet);
	}

	/**
	 * Send an ICMP packet to the destination
	 * @param icmpType the ICMP Type field of the ICMP packet
	 * @param icmpCode the ICMP Code field of the ICMP packet
	 * @param payload the payload of the ICMP packet, can be null to indicate 4 zero bytes
	 * @param dstIp the destination of the IP packet
	 * @param srcIp the source of the IP packet, can be 0 to select one automatically
	 */
	public void sendPacketICMP(byte icmpType, byte icmpCode, IPacket payload, int dstIp, int srcIp)
	{
		ICMP icmpPacket = new ICMP();
		icmpPacket.setIcmpType(icmpType);
		icmpPacket.setIcmpCode(icmpCode);
		icmpPacket.resetChecksum();
		if(payload == null)
		{
			Data dataPayload = new Data();
			dataPayload.setData(new byte[4]);
			icmpPacket.setPayload(dataPayload);
		}
		else
		{
			icmpPacket.setPayload(payload);
		}
		sendPacketL4(icmpPacket, IPv4.PROTOCOL_ICMP, dstIp, srcIp);
	}

	/**
	 * Handle the failure occurred when sending packet in L2 (link layer), usually called when an ARP lookup fails
	 *
	 * @param etherPacket the Ethernet packet about to send
	 */
	public void handleFailureL2(Ethernet etherPacket)
	{
		if(etherPacket.getEtherType() == Ethernet.TYPE_IPv4)
		{
			IPv4 ipv4Packet = (IPv4) etherPacket.getPayload();
			handleFailure(ipv4Packet, ICMP.TYPE_UNREACHABLE, ICMP.CODE_HOST_UNREACHABLE);
		}
	}

	/**
	 * Handle an ARP packet received on a specific interface.
	 *
	 * @param etherPacket the complete ARP packet that was received
	 * @param inIface     the interface on which the packet was received
	 */
	private void handleArpPacket(Ethernet etherPacket, Iface inIface)
	{
		// Make sure it's an ARP packet
		if (etherPacket.getEtherType() != Ethernet.TYPE_ARP)
		{
			return;
		}

		// Get ARP header
		ARP arpPacket = (ARP) etherPacket.getPayload();
		int targetIp = ByteBuffer.wrap(arpPacket.getTargetProtocolAddress()).getInt();

		switch (arpPacket.getOpCode())
		{
		case ARP.OP_REQUEST:
			// Check if request is for one of my interfaces
			if (targetIp == inIface.getIpAddress())
			{
				this.arpCache.sendArpReply(etherPacket, inIface);
			}
			break;
		case ARP.OP_REPLY:
			// Check if reply is for one of my interfaces
			if (targetIp != inIface.getIpAddress())
			{
				break;
			}

			// Update ARP cache with contents of ARP reply
			int senderIp = ByteBuffer.wrap(arpPacket.getSenderProtocolAddress()).getInt();
			ArpRequest request = this.arpCache.insert(new MACAddress(arpPacket.getSenderHardwareAddress()), senderIp);

			// Process pending ARP request entry, if there is one
			if (request != null)
			{
				for (Ethernet packet : request.getWaitingPackets())
				{
					/***************************************************/
					/* send packet waiting on this request             */
					/***************************************************/
					packet.setDestinationMACAddress(arpPacket.getSenderHardwareAddress());
					sendPacket(packet, request.getIface());
				}
			}
			break;
		}
	}
}
