package edu.wisc.cs.sdn.sr;

import net.floodlightcontroller.packet.*;

/**
 * Implements RIP.
 *
 * @author Anubhavnidhi Abhashkumar and Aaron Gember-Jacobson
 */
public class RIP implements Runnable
{
	public static final int RIP_MULTICAST_IP = 0xE0000009;

	/**
	 * Send RIP updates every 10 seconds
	 */
	private static final int UPDATE_INTERVAL = 10;

	/**
	 * Timeout routes that neighbors last advertised more than 30 seconds ago
	 */
	private static final int TIMEOUT = 30;

	private static final int MAX_METRIC = 16;

	/**
	 * Router whose route table is being managed
	 */
	private Router router;

	/**
	 * Thread for periodic tasks
	 */
	private Thread tasksThread;

	public RIP(Router router)
	{
		this.router = router;
		this.tasksThread = new Thread(this);
	}

	public void init()
	{
		// If we are using static routing, then don't do anything
		if (this.router.getRouteTable().getEntries().size() > 0)
		{
			return;
		}

		System.out.println("RIP: Build initial routing table");
		for (Iface iface : this.router.getInterfaces().values())
		{
			this.router.getRouteTable().addEntry((iface.getIpAddress() & iface.getSubnetMask()), 0, // No gateway for subnets this router is connected to
					iface.getSubnetMask(), iface.getName());
		}
		System.out.println("Route Table:\n" + this.router.getRouteTable());

		this.tasksThread.start();

		/***************************************************************/
		/* Add other initialization code as necessary                  */
		/***************************************************************/
	}

	/**
	 * Handle RIP packet received by the router
	 * @param udpPacket the received UDP packet
	 * @param srcIp the source IP of the RIP packet
	 * @param inIface the interface on which the packet was received
	 */
	public void handlePacket(UDP udpPacket, int srcIp, Iface inIface)
	{
		RIPv2 ripPacket = (RIPv2) udpPacket.getPayload();

		/***************************************************************/
		/* Handle RIP packet                                           */
		/***************************************************************/

		if(ripPacket.getCommand() != RIPv2.COMMAND_RESPONSE)
			return;

		for(RIPv2Entry entry : ripPacket.getEntries())
		{
			// Split Horizon
			if(entry.getNextHopAddress() != 0 && router.isLocalAddress(entry.getNextHopAddress()))
				continue;
			if(entry.getMetric() >= MAX_METRIC)
				continue;

			router.getRouteTable().updateEntry(entry.getAddress(), entry.getSubnetMask(), srcIp, inIface.getName(), entry.getMetric() + 1, TIMEOUT);
		}

		System.out.println("Received RIP packet, Route Table:\n" + this.router.getRouteTable());
	}

	/**
	 * Perform periodic RIP tasks.
	 */
	@Override
	public void run()
	{
		/***************************************************************/
		/* Send period updates and time out route table entries        */
		/***************************************************************/

		while(true)
		{
			onTimer();

			try
			{
				Thread.sleep(UPDATE_INTERVAL * 1000);
			}
			catch(InterruptedException e)
			{
				break;
			}
		}
	}

	private void onTimer()
	{
		router.getRouteTable().timeEntries(UPDATE_INTERVAL);

		// TODO: split into multiple packets when there are too many entries
		RIPv2 ripPacket = new RIPv2();
		router.getRouteTable().foreachEntry((RouteTableEntry routeEntry) ->
		{
			if (routeEntry.getMetric() >= MAX_METRIC)
				return;
			RIPv2Entry ripEntry = new RIPv2Entry();
			ripEntry.setAddressFamily(RIPv2Entry.ADDRESS_FAMILY_IPv4);
			ripEntry.setAddress(routeEntry.getDestinationAddress());
			ripEntry.setSubnetMask(routeEntry.getMaskAddress());
			ripEntry.setNextHopAddress(routeEntry.getGatewayAddress());
			ripEntry.setMetric(routeEntry.getMetric());
			ripPacket.addEntry(ripEntry);
		});
		ripPacket.setCommand(RIPv2.COMMAND_RESPONSE);

		UDP udpPacket = new UDP();
		udpPacket.setSourcePort(UDP.RIP_PORT);
		udpPacket.setDestinationPort(UDP.RIP_PORT);
		udpPacket.setPayload(ripPacket);

		router.sendPacketL4(udpPacket, IPv4.PROTOCOL_UDP, RIP_MULTICAST_IP, 0);
	}
}
